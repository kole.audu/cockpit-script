var express = require('express');
var router = express.Router();
const axios = require('axios');

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/post-products', function(req, res, next) {
  axios.get('https://naturalshop.ca/api/products')
  .then(async (response) => {
    // console.log('response: ', response.data[0]);
    console.log('responseLength: ', response.data.length);
    for(var i=0; i<response.data.length; i++){
      var data = response.data[i]
      data.id = data._id
      delete data._id
      await axios.post('http://localhost/cockpit-core/api/content/item/product',{
        method:'POST',
        headers: {
          'api-key': "API-936bb921243ccf1939809b291200b3a18208f9fc",
          "Content-type": "application/json",
        },
        data
      }).then(res => {
        console.log('status: ', res.status)
        console.log(res.data)
      }).catch(err => {
          console.log('error:', err)
          console.log(err.response)
          return res.send(err)
      })
    }
    return res.status(200).json('Successfull');
  })
  .catch(err => {
    console.log(err)
    return res.send(err)
  });
});

module.exports = router;